#! /bin/bash
# Mettre cette variable à 1 si on veut que le corbeau soit actif.
ACTIF=0

TEMP="$(mktemp)"
trap "rm -f ${TEMP}" EXIT

# On désactive spamassassin car il semble y avoir des problèmes
# (de toute façon, c'est pas super utile vu l'autre vérification)
# spamassassin -e -p /etc/spamassassin/corbeau.conf > ${TEMP}

cat > ${TEMP}

# Est-ce que le corbeau est actif ?
if [[ $ACTIF == 1 ]]; then

  # Est-ce du spam ?
  if [[ $? == 0 ]]; then

    # Est-ce que ça contient le mot de passe ?
    if cat ${TEMP} | egrep -q "^Mot de passe : corbeau$"; then
      # On loggue tout dans /var/log/corbeau
      DIR="/var/log/corbeau"
      FILE="corbeau_$(date +%Y_%m_%d_%H_%M_%S)";

      # Création d'une clef AES aléatoire de 256 bits.
      dd if=/dev/urandom bs=32 count=1 | sha256sum | head -c 32 >> ${DIR}/keys/${FILE}.key 2>> /var/log/corbeau/corbeau.err

      # Chiffrement du mail venant vers corbeau@crans.org à l'aide de la clef
      cat ${TEMP} | gpg --armour --symmetric --cipher-algo aes256 --passphrase $(cat ${DIR}/keys/${FILE}.key) >> ${DIR}/${FILE}.log 2>> /var/log/corbeau/corbeau.err

      # Cf ssss, l'idée est de finir par splitter la clef AES à l'aide de ssss-split, et d'en distribuer un bout à chaque membre du C.A., il faudrait au minimum #ca/2 bouts de la clef pour la reconstruire, et débaguer le corbeau.
 
      # Puis on envoit la version modifiée.
      cat ${TEMP} | egrep -v "^Mot de passe : corbeau$" | \
      formail -I "Received" \
              -I "From" \
              -I "Sender" \
              -I "DKIM-Signature" \
              -I "DomainKey-Signature" \
              -I "X-Google-Sender-Auth" \
              -I "X-Original-To" \
              -I "X-Virus-Scanned" \
              -I "X-Greylist" \
              -I "Delivered-To" \
              -I "To" \
              -I "Message-ID" \
              -I "User-Agent" \
              -I "X-Newsreader" \
              -I "Organization" \
              -I "Return-Path" \
              -A "Message-Id: $(date '+<corbeau.%s@crans.org>')" \
              -A "From: corbeau@crans.org" \
              -A "Newsgroups: crans.radio-ragots" \
              -A "Path: Corbeau" | \
       rnews 2>> /var/log/corbeau/corbeau.err

     # Le mail ne contient pas le mot de passe, on répond à l'expéditeur
    else
      ( cat ${TEMP} | formail -r -A "From: corbeau@crans.org" -A "Content-Type: text/plain; charset=UTF-8; format=flowed" ; cat << EOF
Bonjour,

Ton mail n'est pas valide car il manque la ligne suivante :
"Mot de passe : corbeau"

Pour plus d'informations :
http://wiki.crans.org/VieCrans/ForumNews/LeCorbeau

-- 
Le corbeau
EOF
) | /usr/sbin/sendmail -t
    fi
  fi

# Corbal inactif, on répond à l'expéditeur.
else
  ( cat ${TEMP} | formail -r -A "From: corbeau@crans.org" -A "Content-Type: text/plain; charset=UTF-8; format=flowed" ; cat << EOF
Bonjour,

Le corbeau est actuellement désactivé. Il est donc inutile d'essayer de t'en servir.

En te remerciant de ta compréhension,

-- 
Le corbeau, désactivé.
EOF
) | /usr/sbin/sendmail -t
fi
