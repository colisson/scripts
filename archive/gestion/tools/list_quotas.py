#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
from commands import getoutput
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap, decode

db = crans_ldap()
comptes = db.search('aid=*')['adherent'] + db.search('cid=*')['club']

txts = []

for c in comptes :
    if not c.compte() :
        continue
    quota = getoutput('quota %s | grep "/dev/" | awk \'{ print $1" "$3" "$4 }\'' % c.compte())
    if quota != '/dev/sda1 400000 600000\n/dev/sda2 102400 153600' and quota != '/dev/sda2 102400 153600\n/dev/sda1 400000 600000' :
        txts.append(u'%s (%s)\n%s' % (c.Nom(), c.compte(), quota))

print '\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(txts)
