#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys

# deprecated
print "Vieux script : veuillez utiliser le script qui se trouve dans le dépôt git du CA."
sys.exit(1)

sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap, decode
db = crans_ldap()

bats = 'ABCGHIJMP'

def titre (bat) :
    if bat == 'P' :
        return 'Pavillon des jardins'
    elif bat == '?' :
        return 'Autres (extérieurs et chambres invalides)'
    else :
        return 'Bâtiment %s' % bat

# création de la liste vide
###########################

liste = {}
for bat in bats + '?' :
    liste[bat] = []

# remplissage de la liste
#########################

# les extérieurs
adhs = db.search( 'paiement=ok&etudes!=Pers' )['adherent']
for adh in adhs :
    bat = adh.chbre()[0]
    if not bat in bats :
        bat = '?'
    liste[bat].append( u'%s %s' % ( adh.nom(), adh.prenom() ) )

# création du fichier tex
#########################

print """\\documentclass[a4paper,11pt]{article}
\\usepackage[T1]{fontenc}
\\usepackage[francais]{babel}
\\usepackage{longtable}
\\usepackage{geometry}
\\usepackage{fancyheadings}
\\geometry{ hmargin=2cm, vmargin=4cm }

\\begin{document}
\\lhead{A.G.O. du CR@NS}
\\rhead{21 avril 2005}
\\pagestyle{fancy}
"""

for bat in bats + '?' :
    # entete du batiment
    print """\\section{%(bat)s}\n
\\chead{\emph{%(bat)s}}
\\begin{longtable}{| p{2.5in} | p{1.5in} |}
\\hline
\\textbf{Nom Prenom} & \\textbf{Signature} \\\\
\\hline
\\endhead""" % {'bat' : titre(bat) }

    # ajout des adhérents
    liste[bat].sort()
    for adh in liste[bat] :
        print (u'%s& \\\\\n\\hline' % adh).encode('iso 8859-15')

    # fin du batiment
    print "\\end{longtable}\n"

print "\\end{document}"
