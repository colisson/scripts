#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-


""" Variables de configuration pour la détection du peer-to-peer """

#: Le traffic udp vers les ip (v4 et v6) des hotes dans cette liste est bloqué
udp_torrent_tracker = [
        'tracker.openbittorrent.com',
        'tracker.ccc.de',
        'tracker.istole.it',
        'tracker.publicbt.com',
        'tracker.1337x.org',
        'fr33domtracker.h33t.com',
        'tracker.torrentbox.com',
         '11.rarbg.com',
         'exodus.desync.com',
         'fr33dom.h33t.com',
         'torrentbay.to',
         'tracker.ex.ua',
         'tracker.ilibr.org',
         'tracker.prq.to',
         'tracker.publichd.eu',
         'tracker.yify-torrents.com',
        ]

#: Limite de débit pour l'ensemble du p2p classifié, en octets/s
#: identique en upload et download
debit_max = 1000000 # = 1 Mb/s = 125 ko/s
debit_adh = 12000 # = 12 Kb/s = 1500 o/s (>= MTU [1500 bytes])

#: Limite de paquets acceptés par protocole P2P en deux heures
limite = {'Bittorrent': 100,
            'AppleJuice': 50,
            'SoulSeek': 500,
            'WinMX': 50,
            'eDonkey': 50,
            'DirectConnect': 50,
            'KaZaa': 50,
            'Ares': 50,
            'GNUtella': 50 }

#: Envoi des mails à disconnect@
disconnect_mail = True

#: Expéditeur des mails de déconnexion
expediteur = "disconnect@crans.org"

