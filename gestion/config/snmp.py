#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
#
PRELOAD_MIBS = (
    "STATISTICS-MIB",
    "SNMPv2-SMI",
    "SNMPv2-MIB",
    "IF-MIB",
    "CONFIG-MIB",
)
