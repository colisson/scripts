#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

SAP_MCAST_GRP = '224.2.127.254' # sap.mcast.net
SAP_MCAST_PORT = 9875
SAP_FILE_TXT = "/var/lib/tv/sap.txt"
SAP_FILE_PIC = "/var/lib/tv/sap.pickel"

BASE_IMAGE_URL = "https://tv.crans.org/images/"
IMAGE_SUFFIX = ".jpg"
SMALL_IMAGE_SUFFIX = "_petites.jpg"

