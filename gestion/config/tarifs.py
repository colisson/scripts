#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Format lisible et importable par la macro wiki des tarifs d'impression """

import impression

tarifs = {
    u"Coût d'un recto A4 couleurs" : impression.c_a4 + impression.c_face_couleur,
    u"Coût d'un recto A4 noir et blanc" : impression.c_a4 + impression.c_face_nb,
    u"Coût d'un recto-verso A4 couleurs" : impression.c_a4 + 2 * impression.c_face_couleur,
    u"Coût d'un recto-verso A4 noir et blanc" : impression.c_a4 + 2 * impression.c_face_nb,

    u"Coût d'un recto A3 couleurs" : impression.c_a3 + impression.c_face_couleur,
    u"Coût d'un recto A3 noir et blanc" : impression.c_a3 + impression.c_face_nb,
    u"Coût d'un recto-verso A3 couleurs" : impression.c_a3 + 2 * impression.c_face_couleur,
    u"Coût d'un recto-verso A3 noir et blanc" : impression.c_a3 + 2 * impression.c_face_nb,

    u"Prix d'une agrafe" : impression.c_agrafe,
 }
