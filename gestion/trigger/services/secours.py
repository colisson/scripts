#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# Service in charge of firewall for trigger.
# Contains multiple subservices for each special
# part of firewall.
#
# Author  : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3
# Date    : 15/06/2014
"""
Firewall service module. is uses the firewall library as it's, it
is not designed to replace it, just to call specific functions from
it to regenerate what needs to.
"""

import cranslib.clogger as clogger
import gestion.config.trigger as trigger_config
logger = clogger.CLogger("trigger", "secours", trigger_config.log_level, trigger_config.debug)

import lc_ldap.shortcuts

from gestion.trigger.host import record_service
import gestion.trigger.firewall4.firewall4 as firewall4

@record_service()
def secours(ob_id, operations=()):
    """Regens the specific service

    """
    if len(operations) != 2:
        logger.warning("Received operations %r, this format is incorrect, discarding.", operations)
        return
    (service, data) = operations
    logger.info("Calling service %s for data %r", service, data)
