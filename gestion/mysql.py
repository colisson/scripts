# -*- mode: python; coding: utf-8 -*-
import threading
import MySQLdb
import collections

def make_cursor(name, config):
    """
    Retourne une classe à utiliser comme un contextmanager, par exemple : 
    cusor = make_cursor("mon_joli_nom", {
        "user": "username",
        "passwd": "*****",
        "db":"database",
        "host":"localhost",
        "charset":'utf8'
    })
    with cusor() as cur:
        cur.execute("UPDATE ...")

    """
    newclass = type(name, (_cursor,), {'_db': collections.defaultdict(lambda: MySQLdb.connect(**config))})
    return newclass

class _cursor(object):
    @classmethod
    def get_db(cls):
        return cls._db[threading.current_thread()]

    @classmethod
    def set_db(cls, value):
        cls._db[threading.current_thread()] = value

    @classmethod
    def del_db(cls):
        try:
            cls._db[threading.current_thread()].close()
        except:
            pass
        try:
            del cls._db[threading.current_thread()]
        except KeyError:
            pass

    def __enter__(self):
        self.cur = self.get_db().cursor()
        try:
            self.cur.execute("DO 0")
        except MySQLdb.Error as error:
            # MySQL server has gone away, probably a timeout
            if error.args[0] == 2006:
                self.del_db()
                self.cur.close()
                self.cur = self.get_db().cursor()
        return self.cur

    def __exit__(self, exc_type, exc_value, traceback):
        self.cur.close()
        self.get_db().commit()

