#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# Licence : GPLv2

import sys, smtplib, commands
from gestion.gen_confs import gen_config
from email.mime.text import MIMEText

import affichage

from lc_ldap import shortcuts
from lc_ldap import printing

smtpserv = "smtp.crans.org"

class mail:
    """
    Envoie un mail à toutes les personnes de la liste 'To', avec les
    informations détaillées des objets contenus dans 'objets'
    (instances des classes Adherent, Machine ou Club) """

    From = 'roots@crans.org'
    To = [ 'roots@crans.org' ]
    Subject = "Surveillance modifications de la base LDAP"

    mail_template =  """From: %(From)s
To: %(To)s
Subject: %(Subject)s
Mime-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Disposition: inline
Content-Transfer-Encoding: 8bit


%(Text)s"""

    # Avec les caractères d'échappement qui vont bien pour la couleur ?
    couleur = False

    def __init__(self,recherches) :
        self.recherches = recherches

    def reconfigure(self) :
        """ Envoi le mail """
        animation = affichage.Animation(texte="Mail de notification de modifications", nb_cycles=len(self.recherches), couleur=True, kikoo=True)

        db = shortcuts.lc_ldap_test()
        details = []
        vus = []
        for rech in self.recherches :
            for results in db.search(unicode(rech)):
                if results.dn in vus : continue
                vus.append(results.dn)
                details.append(printing.sprint(results))
            animation.new_step()

        texte = '\n\n- - - - = = = = # # # # # # = = = = - - - -\n\n'.join(details)

        if not details :
            print "rien"
            return

        print affichage.OK

        print '\nEnvoie Mail'
        if not self.couleur :
            import re
            texte = re.sub('\x1b\[1;([0-9]|[0-9][0-9])m','',texte)

        conn=smtplib.SMTP(smtpserv)
        msg = MIMEText(texte.encode('utf-8'), _charset='utf-8')
        msg['From'] = self.From
        msg['To'] = ','.join(self.To)
        msg['Subject'] = self.Subject
        conn.sendmail(self.From, self.To , msg.as_string())
        conn.quit()
        print affichage.OK

class mail_solde:
    """
    Envoie un mail a la ML impression pour les modifications de solde"""

    From = 'root@crans.org'
    To = [ 'impression@crans.org' ]
    Subject = "Modification de solde"

    mail_template =  """From: %(From)s
To: %(To)s
Subject: %(Subject)s
X-Mailer: modif_solde (/usr/scripts/gestion/gen_confs/supervision.py)

%(Text)s"""

    # Avec les caractères d'échappement qui vont bien pour la couleur ?
    couleur = False

    def __init__(self,modifs) :
        self.modifs = modifs

    def reconfigure(self) :
        """ Envoi le mail """
        animation = affichage.Animation(texte="Mail de notification de modifications du solde", nb_cycles=len(self.modifs), couleur=True, kikoo=True)
        texte = ''
        for modif in self.modifs:
            animation.new_step()
            texte = texte + modif.decode('utf-8', 'ignore') + u'\n'

        print('\tEnvoi mail')

        conn=smtplib.SMTP(smtpserv)
        conn.sendmail(self.From, self.To , \
             self.mail_template % { 'From' : self.From,
                                    'To' : ','.join(self.To),
                                    'Subject' : self.Subject,
                                    'Text' : texte.encode('utf-8', 'ignore')  } )
        conn.quit()
        print affichage.OK
