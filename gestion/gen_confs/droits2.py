#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
"""Service gérant les modifications de droits LDAP d'utilisateurs
et proposant les outils pour répercuter ces modifications sur
les listes de diffusion et les groupes unix associés."""

import sys
import ldap

import argparse

# Imports pour LDAP
import gestion.config as config
from gestion.gen_confs import gen_config
from gestion import affichage

from lc_ldap import shortcuts
from lc_ldap.crans_utils import strip_accents

try:
    sys.path.append('/usr/lib/mailman')
    from Mailman import MailList
    from Mailman import Utils
    from Mailman.UserDesc import UserDesc
except:
    # Machine sans mailman, les ML ne seront pas reconfigurées
    pass

class droits(gen_config):
    def restart(self):
        # Rien à faire
        pass

    def __str__(self):
        return "droits"

CONN = shortcuts.lc_ldap_admin()

class droits_ldap(droits):
    ####### Les groupes
    base_group_dn = 'ou=Group,dc=crans,dc=org'

    def build_group(self):
        """ Reconstruit les groupes dans la base LDAP """
        animation = affichage.Animation(texte="Reconfiguration des groupes unix", nb_cycles=len(config.droits_groupes.keys()), couleur=True, kikoo=True)
        for (group, droits_groupe) in config.droits_groupes.items():
            animation.new_step()

            # Qui doit être dans ce groupe ?
            # Construction du filtre LDAP
            droits_groupe = [
                "(droits=%s)" % (droit_groupe,)
                for droit_groupe in droits_groupe
            ]

            res = CONN.search(u'(|%s)' % (
                "".join(droits_groupe),
            ))

            # Récupération de la constitution du groupe actuel
            dn = 'cn=%s,%s' % (group, self.base_group_dn)
            data = CONN.search_s(dn, 0, 'objectClass=posixGroup')[0][1]
            init_data = data.copy()

            # Garantit l'unicité
            uid_set = set([
                unicode(adher['uid'][0]).encode(config.ldap_encoding)
                for adher in res
            ])

            for _user in config.static_users_groups.get(group, []):
                uid_set.add(_user)

            # On remplace la liste de membres actuelle.
            data['memberUid'] = list(uid_set)

            # Sauvegarde
            modlist = ldap.modlist.modifyModlist(init_data, data)
            CONN.modify_s(dn, modlist)

    def config_ML(self, mode='autosync', args=None):
        """Reconfigure les MLs.
        mode désigne le mode de fonctionnement :
         * autosync est le mode par défaut, il resynchronise les MLs avec les
            droits
         * forcedel fait comme autosync, en demandant un args du type
            [('ml1', 'all'), ('ml2', 'adresse_mail1')]
            et supprime les adresses mail concernées de la mailing-liste,
            'all' pour toutes les MLs ne correspondant pas à des gens avec
            des droits (seulement si elles ne seront pas réinscrites
            automatiquement)
         * getunsync retourne les gens qui devraient être inscrits/désinscrits
            de chaque ML
        args désigne l'argument au mode choisi."""

        if mode == 'getunsync':
            unsync = {}
            label = u'Recherche des ml non synchronisées'
        else:
            label = u'Reconfiguration des ml Cr@ns'

        try:
            animation = affichage.Animation(texte=label, nb_cycles=len(config.droits_mailing_listes.keys()), couleur=True, kikoo=True)
        except:
            pass

        for (liste, droits_liste) in config.droits_mailing_listes.items():
            try:
                animation.new_step()
            except:
                pass

            if liste[0] == '+':
                liste = liste[1:]
                __only_add = True
            else:
                __only_add = False

            # Instance correspondant à la ML
            mlist = MailList.MailList(liste)
            self.mlist_to_unlock = mlist

            # Qui doit être dans cette ML ?
            # Construction du filtre LDAP
            droits_liste = [
                "(droits=%s)" % (liste_droit,)
                for liste_droit in droits_liste
            ]

            res = CONN.search(u'(|%s)' % (
                "".join(droits_liste),
            ))

            # Construit la liste des emails à partir des résultats
            to_add_list = [
                (
                    unicode(adher['mail'][0]).lower(),
                    u"%s %s" % (
                        unicode(adher['prenom'][0]),
                        unicode(adher['nom'][0]),
                    ),
                )
                for adher in res
            ]

            # Liste des personnes déja inscrites
            deja_inscrits = {
                addr.lower(): addr
                for addr in mlist.getMemberCPAddresses(mlist.getMembers())
            }

            # Construit la liste des emails qu'il faut vraiment ajouter.
            to_add = [
                tpl
                for tpl in to_add_list
                if tpl[0] not in deja_inscrits
            ]

            # On stocke également les mails des gens qui étaient déjà inscrits
            # mais qui n'ont plus les droits qui vont bien. Ils seront enlevés
            # sous certaines conditions
            inscrits_non_reinscrits = {
                key: value
                for (key, value) in deja_inscrits.iteritems()
                if key not in [
                    elem[0]
                    for elem in to_add_list
                ]
            }

            if mode == 'autosync' or mode == 'forcedel':
                # On ajoute tout ce qu'il y a dans to_add
                for (mail, nom) in to_add:
                    pw = Utils.MakeRandomPassword()
                    userdesc = UserDesc(mail, strip_accents(nom), pw)
                    mlist.ApprovedAddMember(userdesc)

                # Selon les cas, on vire le surplus
                if not __only_add or mode == 'forcedel':
                    # Supression des personnes inscritees en trop
                    if not __only_add or (mode == 'forcedel' and args != None and (liste, 'all') in args):
                        for mail in inscrits_non_reinscrits.values():
                            mlist.ApprovedDeleteMember(mail)
                    else:
                        # Suppression des mails demandés en argument dans la fonction
                        for mail in inscrits_non_reinscrits.values():
                            if (liste, mail) in args:
                                mlist.ApprovedDeleteMember(mail)

                mlist.Save()
            elif mode == 'getunsync':
                if to_add or inscrits_non_reinscrits:
                    unsync[liste] = (
                        __only_add,
                        [
                            elem[1]
                            for elem in to_add
                        ],
                        inscrits_non_reinscrits.values()
                    )

            mlist.Unlock()
            self.mlist_to_unlock = None

        if mode == 'getunsync':
            return unsync

    def gen_conf(self):
        try:
            self.build_group()
            print affichage.OK
        except:
            print affichage.ERREUR
            if self.debug:
                import traceback
                traceback.print_exc()

        try:
            self.config_ML()
            print affichage.OK
        except:
            print affichage.ERREUR
            if self.debug:
                import traceback
                traceback.print_exc()
            try:
                # Au cas où...
                self.mlist_to_unlock.Unlock()
            except:
                pass

class desabonner_ml(droits_ldap):
    def gen_conf(self):
        animation = affichage.Animation(texte="Désabonnnement ml Cr@ns", nb_cycles=1, couleur=True, kikoo=True)
        try:
            to_del = []
            for arg in self.args:
                to_del.append(arg.split('$'))
            self.config_ML(mode='forcedel', args=to_del)
            animation.new_step()
            print affichage.OK
        except:
            print affichage.ERREUR
            raise

def print_liste(droit):
    """Donne la liste des membres actifs """
    for adh in CONN.search(u'(&(droits=%s)(!(chbre=EXT)))' % (droit,)):
        print "%s %s" % (adh['nom'][0], adh['prenom'][0])

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Répercussion des droits Crans sur les groupes Unix et les listes de diffusion.", add_help=False)
    PARSER.add_argument('-h', '--help', help="Affiche ce message et quitte", action="store_true")
    PARSER.add_argument('-d', '--delete', nargs="+", help="Liste des adresses à retirer des listes de diffusion, au format ``ml1:mail1,mail2 ml2:all ml3:mail2,mail4 ...''", type=str, action="store")
    PARSER.add_argument('-p', '--print_acl', nargs="+", help="Afficher les membres actifs disposant des droits fournis au format ``droit1 droit2 droit3 ...''", type=str, action="store")
    PARSER.add_argument('-u', '--unsync', help="Affiche la liste des listes de diffusion avec des gens à inscrire ou enlever", action="store_true")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    CL = droits_ldap()

    if ARGS.delete:
        TO_DEL = []
        for ARG in ARGS.delete:
            if not ':' in ARG:
                continue
            (NOM_LISTE, MAILS) = ARG.split(':', 1)
            TO_DEL.extend([(NOM_LISTE, MAIL) for MAIL in MAILS.split(',')])
        print u"Suppression forcée de %s" % (str(TO_DEL),)
        CL.config_ML(mode='forcedel', args=TO_DEL)

    if ARGS.print_acl:
        for DROIT in ARGS.print_acl:
            # Affichage des membres ayant le droit ``DROIT''
            TITRE = "%s : " % (DROIT,)
            print u"-" * len(TITRE)
            print_liste(DROIT)

    if ARGS.unsync:
        print "Listes de diffusion non synchronisées"
        UNSYNCED = CL.config_ML(mode="getunsync")
        print
        for (NOM_LISTE, (ONLY_ADD, TO_ADD, TO_DEL)) in UNSYNCED.iteritems():
            TITRE = u"Liste %s :" % (NOM_LISTE,)
            print TITRE
            print u"=" * len(TITRE)
            print u"À ajouter : %s" % (u", ".join(TO_ADD),)
            print u"À enlever : %s" % (u", ".join(TO_DEL),)
            if ONLY_ADD:
                print u"(Les adresses à enlever ne le seront pas automatiquement)\n"
            else:
                print "\n"
