#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Script de mise a jour des firmwares de switchs version2

Fonction:
    - display, affiches les états de mise à jour
    - upgrade, effectue les mises à jour
    - reload
    - stats, affiche des jolies stats

Attention, le fichier firmware doit se trouver dans le sous dossier
non gité firmware_path (cf ci dessous).

Le fichier de config hp_switchs doit également faire référence
à la version à jour du firmware.

Gabriel Detraz 2017
"""

from __future__ import print_function

import sys
import subprocess
from annuaires_pg import all_switchs
import hptools2
import pexpect
import affichage
from config.hp_switchs import hp_procurve_map, all_models
import argparse
import os.path
from lc_ldap import shortcuts

switchs = all_switchs()

# Dossier des firmwares
firmware_path = '/usr/scripts/firmwares_switchs/'

def get_version(switch):
    sw = hptools2.HPSwitch(switch)
    all_version = sw.version()
    firmware = all_version.split('revision ')[1].split(',')[0]
    model = next(x for x in all_version.split() if x.startswith('J'))
    return firmware, model

def check_version(switch, check_only=False):
    firmware, model = get_version(switch)
    try:
        normal_firm = hp_procurve_map[model[:-1]][0]
    except KeyError:
        if check_only:
            print("Référence inconnue dans le mapping")
        return True
    return normal_firm == firmware

def update_ldap_header(ldap, hostname):
    try:
        switch = ldap.search(
            u'(&(host=%s)(objectClass=switchCrans))' % hostname,
            mode='rw'
        )[0]
        ldap_header, header_line = None, None
        for line, com in enumerate(switch['info']):
            if com.value.startswith(';'):
                ldap_header = com.value.encode('utf-8')
                header_line = line
        firmware, model = get_version(hostname)
        correct_header = "; %s Configuration Editor; Created on release #%s" % (model, firmware)
        if not ldap_header or ldap_header != correct_header:
            print("Le header ldap n'est pas correct, il devrait être %s" % correct_header)
            query = raw_input('Correction dans ldap ? (Y pour confirmer)')
            if query == "Y":
                with switch as sw:
                    if header_line:
                        sw['info'][header_line] = unicode(correct_header)
                    else:
                        sw['info'].append(unicode(correct_header))
                    sw.history_gen()
                    sw.save()
    except:
        print(
            (
                u"Assurez-vous que le switch est bien connecté "
                u"au réseau pour générer automatiquement son header..."
            ).encode('utf-8'),
            file=sys.stderr
        )

def do_stats(switchs):
    stats = dict()
    for model in all_models:
        stats[model] = [0, 0]
    for sw in switchs:
        firmware, model = get_version(sw)
        if check_version(sw):
            stats[hp_procurve_map[model[:-1]][1]][0] += 1
        else:
            stats[hp_procurve_map[model[:-1]][1]][1] += 1
    return stats

def do_upgrade(switch, destination='primary'):
    if not check_version(switch, check_only=False):
        query = raw_input('Mise à jour de %s (Y pour confirmer)' % switch)
        if query == "Y":
            firmware, model = get_version(switch)
            try:
                normal_firm = hp_procurve_map[model[:-1]][0]
            except KeyError:
                print("Référence inconnue dans le mapping")
                return
            firmware_long_path = firmware_path + normal_firm.replace('.','_') + '.swi'
            if not os.path.isfile(firmware_long_path):
                print("Firmware %s introuvable !" % normal_firm)
                return
            print('sudo scp %s root@%s:os/%s' % (firmware_long_path, switch, destination))
            subprocess.call(['/usr/bin/sudo', '/usr/bin/scp', firmware_long_path, 'root@%s:os/%s' % (switch, destination)])
        else:
            return

def do_reload(switch):
    query = raw_input('Reload de %s pour application de maj (Y pour confirmer)' % switch)
    if query == "Y":
        return
    else:
        return

def do_reflash(switch):
    query = raw_input('Rechargement de la conf de %s (Y pour confirmer)' % switch)
    if query == "Y":
        sw = switch.split('.')[0]
        subprocess.call(['/usr/scripts/gestion/gen_confs/switchs2.py', sw,], stdout=open('/tmp/%s.conf' % sw, "w"))
        subprocess.call(['/usr/bin/sudo', '/usr/bin/scp', '/tmp/%s.conf' % sw, 'root@%s:cfg/startup-config' % switch])

parser = argparse.ArgumentParser()
parser.add_argument('--stats', action="store_true", help="Affiche des statistiques générales sur les switchs et leur état de mise à jour")
parser.add_argument('--display', action="store_true", help="Affiche l'etat de maj de l'ensemble des switchs")
parser.add_argument('--allupgrade', action="store_true", help="Mets à jour les switchs, demande confirmation")
parser.add_argument('--upgrade', help="Mets à jour ce switch, demande confirmation")
parser.add_argument('--secondary', action="store_true", help="Mets à jour l'image secondaire des switchs, demande confirmation")
parser.add_argument('--reload', action="store_true", help="Reload pour appliquer la mise à jour. ATTENTION, doit être lancé une fois que le switch est complétement flashé ! (5mn)")
parser.add_argument('--config', help="Mets à jour la config du switch")
parser.add_argument('--allconfig', action="store_true", help="Mets à jour la config de tous les switchs")

if __name__ == '__main__':
    args = parser.parse_args()
    if args.stats:
        stats = do_stats(switchs)
        print(affichage.tableau(stats.values(), titre=["Switchs à jour", "Switch pas à jour"],  alignement=['d', 'd']))
    if args.display:
        for sw in switchs:
            print(sw)
            print(get_version(sw))
            if check_version(sw, check_only=True):
                print("A jour")
            else:
                print("A upgrader")
    if args.allupgrade:
        for sw in switchs:
            if args.secondary:
                do_upgrade(sw, destination='secondary')
            else:
                do_upgrade(sw)
    if args.upgrade:
        switch = args.upgrade + '.switches.crans.org'
        if switch in switchs:
            if args.secondary:
                do_upgrade(switch, destination='secondary')
            else:
                do_upgrade(switch)
        else:
            print("Ce switch n'existe pas")
    if args.allconfig:
        ldap = shortcuts.lc_ldap_admin()
        for sw in switchs:
            update_ldap_header(ldap, sw)
            do_reflash(sw)
    if args.config:
        ldap = shortcuts.lc_ldap_admin()
        switch = args.config + '.switches.crans.org'
        if switch in switchs:
            update_ldap_header(ldap, switch)
            do_reflash(switch)
        else:
            print("Ce switch n'existe pas")
    if args.reload:
        query = raw_input('Etes-vous certain de reloader les switchs maintenant ? (Y pour oui)')
        if query == "Y":
            for sw in switchs:
                # Applique sur les switchs non à jour
                if not check_version(sw):
                    do_reload(sw)
        else:
            print("Prudent, attendons 5 minutes ;)")
