#!/bin/bash

# Renvoie un si le fichier MASTER est présent
# Verifie le statut de quagga, si quagga est down, tente de le relancer
# puis passe en fault (activation du routage sur le second routeur)

if [ -f /etc/keepalived/MASTER ]; then
    if systemctl is-active quagga; then
        exit 0
    else
        systemctl restart quagga
        if systemctl is-active quagga; then
            exit 0
        else
            rm /etc/keepalived/MASTER
            exit 1
        fi
    fi
else
    exit 1
fi
