#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""Efface les machines wifi pas vues dans les logs depuis un certain temps.
Par effacement, on veut dire ici qu'on réalloue le rid"""

import re
import sys
import subprocess
import datetime
import os

from lc_ldap.shortcuts import lc_ldap_admin
from lc_ldap.crans_utils import format_mac

MAC = re.compile('[\-:]'.join('[a-f0-9]{2}' for _ in xrange(6)), re.IGNORECASE)
FILTER = u'(&(objectClass=%s)(ipHostNumber=*))'
BATCH = False

def get_creation_date(machine):
    """Plus vieille ligne d'histo = date de création, en datetime"""
    return min(hist.get_datetime() for hist in machine['historique'])

def get_candidates(realm):
    """Retourne la liste des machines concernées"""
    ldap = lc_ldap_admin()

    print "Search machines"
    machines = ldap.search(FILTER % realm, mode='rw', sizelimit=3000)

    return machines


def do_cleanup(older, machines, logs):
    """Do the actual clean-up, for machines not connected for more than
    `older` days"""

    print "Listing macs ..."
    deadline = datetime.datetime.now() - datetime.timedelta(days=older)
    def is_old(machine):
        """Est-ce une machine créée il y a un moment déjà ? On ne garde
        que celles-ci"""
        return get_creation_date(machine) < deadline
    macs = set(machine['macAddress'][0].value for machine in machines \
        if is_old(machine))
    print "%d macs to test" % len(macs)

    for filename in logs:
        print "Parsing %s" % filename
        mac_seen = set(macs_in_file(filename))
        print "  Diff ..."
        macs.difference_update(mac_seen)

    print "Will reset %d macs" % len(macs)
    if not BATCH:
        print "List incomming"
        raw_input()
        for machine in machines:
            if machine['macAddress'][0].value in macs:
                print machine

    if not BATCH:
        print "Continue ? (^C to stop)"
        raw_input()
    for machine in machines:
        if machine['macAddress'][0].value in macs:
            reset_ip(machine)

def reset_ip(machine):
    """Retire l'ip (et le rid) d'une machine"""
    print "Resetting ip for %r" % machine
    with machine:
        machine['rid'] = []
        machine['ipHostNumber'] = []
        machine.history_gen()
        machine.save()

def macs_in_file(filename):
    """Itérateur des macs dans `filename`"""
    if not os.path.exists(filename):
        raise Exception(u"le fichier %r n'existe pas, êtes-vous sur le bon serveur ?" % filename)
    if filename.endswith('.gz'):
        cat = 'zcat'
    else:
        cat = 'cat'
    proc = subprocess.Popen([cat, filename], stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        for mac in MAC.findall(line):
            yield format_mac(mac)

def file_list(num, folder):
    """Itérateur des noms de fichiers à parser, pour les n derniers jours"""
    base = '/var/log/%s/global.log' % folder
    if num > 0:
        yield base
    for last in xrange(1, num):
        yield "%s.%d.gz" % (base, last)

def filter_filaire(machine):
    """Renvoie True s'il faut garder la machine pour suppression potentielle.
    Certaines machines sont exemptés de suppression, car dans des conditions
    trop spéciale."""

    # On retire les machines des clubs et crans, car pas forcément de radius
    if 'aid=' not in machine.dn:
        return False
    
    # On laisse tranquille les adhérents n'habitant plus sur le campus, car
    # plus de radius
    if unicode(machine.proprio(mode='ro').get('chbre', [u'EXT'])[0]) == 'EXT':
        return False

    # défaut
    return True

if __name__ == '__main__':
    folder = (sys.argv + [''])[1]
    BATCH = "--batch" in sys.argv
    if folder not in ['wifi', 'filaire']:
        print "Not supported"
        exit(1)
    if folder == 'filaire':
        realm = u'machineFixe'
    else:
        realm = u'machineWifi'

    machines = get_candidates(realm)
    if folder == 'filaire':
        print "Filtrage filaire ...."
        machines = filter(filter_filaire, machines)
        
    days = 31*1
    do_cleanup(days, machines, file_list(days, folder))
