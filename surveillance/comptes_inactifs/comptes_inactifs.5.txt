Encoding: utf-8
Subject: [CRANS] Compte inactif depuis plus d'un mois

Bonjour %(nom)s,

Si tu t'es connecté(e) (par ssh, pop, imap ou webmail) dans les trois
dernières heures, tu peux ignorer ce message.

Tu reçois ce mail parce que tu as un compte sur le serveur des adhérents du
CRANS (en tant qu'ancien adhérent), et que tu ne t'es pas connecté(e) depuis
le %(date)s (ou tu ne t'es jamais connecté(e)), ce qui fait plus
d'un mois.

Il y a un fichier .forward dans ton home, ce qui veut dire que tu rediriges
tes mails. Si tu ne te sers plus de ton compte CRANS, ce serait bien de nous
le signaler afin de libérer les ressources qui te sont allouées. Il est
possible de supprimer le compte tout en conservant l'adresse CRANS (en tant
que redirection vers une autre adresse), ainsi que les alias. Cela ne
s'applique qu'aux redirections vers une autre adresse ; si tu utilises
procmail, il faut conserver ton compte. Tu dois aussi conserver ton compte
si tu l'utilises pour héberger ta page web.

Si tu veux arrêter de recevoir ces avertissements, tu dois te connecter au
moins une fois par mois, en utilisant l'un des protocoles suivants :
 - webmail (http://webmail.crans.org)
 - pop (pop.crans.org)
 - imap (imap.crans.org)
 - ssh (ssh.crans.org)

Au bout de six mois d'inactivité (et d'au moins cinq avertissements
similaires), ton compte sera automatiquement supprimé.

Nous te prions de ne pas répondre à ce mail, sauf pour nous signaler une
anomalie ou confirmer un abandon définitif du compte.

-- 
Script de détection des comptes inactifs
