#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import lc_ldap.shortcuts as shortcuts
import lc_ldap.attributs as attributs

if __name__ == "__main__":
    LDAP = shortcuts.lc_ldap_readonly()

    LISTE = LDAP.search(u"(&(droits=*)(!(droits=%s)))" % (attributs.multimachines,))
    print " ".join([unicode(user['uid'][0]) for user in LISTE])
