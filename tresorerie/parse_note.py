#!/usr/bin/env python2.7
# -*- coding: utf-8; mode: python -*-
"""
Requête SQL utilisée pour pondre le csv à parser :

COPY (SELECT transactions.date::date, transactions.description,
transactions.type, transactions.emetteur, transactions.destinataire,
transactions.montant*transactions.quantite, emetteurs.nom,
emetteurs.prenom, destinataires.nom, destinataires.prenom as total
FROM transactions LEFT JOIN comptes AS emetteurs ON
transactions.emetteur = emetteurs.idbde LEFT JOIN comptes AS
destinataires ON transactions.destinataire = destinataires.idbde WHERE
valide=true AND (emetteur=2175 OR destinataire=2175) AND date BETWEEN
'2015-01-01 00:00:00' AND '2016-01-01 00:00:00' ORDER BY id DESC) TO
'/tmp/crans.csv' WITH CSV;
"""

import csv
import argparse

SOLDE = '7062 Crédit solde'
MEMBRES = '410 Membres'
NOTE = '5125 Note kfet'
SOIREE = '77131 Don soirée Crans'
VENTE = '707 Vente de marchandise'

CRANS_ID = 2175
TRANSFERT_DON = ['transfert', 'don']

def parse_file(filepath):
    lines = [
        [
            line[0].replace('-', '/'),
            line[1],
            line[2],
            int(line[3]),
            int(line[4]),
            int(line[5])/100.0
        ] + line[6:]
        for line in csv.reader(open(filepath))
    ]

    account1 = ""
    account2 = ""
    account3 = ""
    description = ""

    newlines = []

    for line in lines:
        # Crédit solde ? Déjà inscrit dans adh_solde.csv en 410-7062, donc
        # il faut juste faire l'entrée note
        if line[4] == CRANS_ID:
            if line[2] in TRANSFERT_DON:
                if "Soirée" in line[1]:
                    account1 = NOTE
                    account2 = SOIREE
                elif "Vente" in line[1]:
                    account1 = NOTE
                    account2 = VENTE
                else:
                    account1 = NOTE
                    account2 = MEMBRES
            else:
                account1 = NOTE
                account2 = "Compte Inconnu"
            description = "%s de %s %s" % (line[1], line[7], line[6])
        else:
            account1 = "Compte Inconnu"
            account2 = NOTE
            description = "%s vers %s %s" % (line[1], line[9], line[8])

        newlines.append([
            line[0], description, line[5], account1, account2
        ])
    return newlines

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Production d'un CSV à partir de la note.", add_help=False)
    PARSER.add_argument('-h', '--help', action='store_true', help='Affiche cette aide et quitte.')
    PARSER.add_argument('filepath', type=str, nargs=1, help="Le fichier csv note à parser")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    DATA = parse_file(ARGS.filepath[0])
    with open("./out.csv", "wb") as CSVFILE:
        CSVWRITER = csv.writer(CSVFILE, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        CSVWRITER.writerow([
            "date",
            "description",
            "amount",
            "account1",
            "account2",
        ])
        for LINE in DATA:
            CSVWRITER.writerow(LINE)
