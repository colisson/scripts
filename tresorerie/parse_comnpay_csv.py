#!/bin/bash /usr/scripts/python.sh
# -*- mode: python ; coding: utf-8 -*-

import csv
import sys
import argparse

COMNPAY = "5127 Compte TPE virtuel ComNPay"
MEMBRES = "410 Membres"
SOGE = "5121 Compte Administration Société Générale"
COMMISSION = "62782 Commission transaction ComNPay"
VOL = "62783 Frais de virement ComNPay"

PAIEMENT_DICT = {
    'minus': [
        MEMBRES,
    ],
    'minus_arg': 'entre',
    'plus': [
        COMMISSION,
        COMNPAY,
    ],
    'plus_arg': 'sort',
    'label': 'Transaction liée à un crédit de solde',
}

TYPE_IDX = 4

TO_LOOK = {
    'date': 14, # Date
    'sort': 15, # Taxe
    'entre': 16, # Entré
}

USEFUL_DATA = {
    'Facture achat CB VAD 3DS' : PAIEMENT_DICT,
    'Encaissement CB VAD' : PAIEMENT_DICT,
    'Facture achat Mastercard VAD 3DS zone SEPA' : PAIEMENT_DICT,
    'Frais sur Virement' : {
        'minus': [
            COMNPAY,
        ],
        'minus_arg': 'sort',
        'plus': [
            VOL,
        ],
        'plus_arg': 'sort',
        'label': 'Commission pour virement vers compte externe',
    },
    'Virement Cpt Externe' : {
        'minus': [
            COMNPAY,
        ],
        'minus_arg': 'sort',
        'plus': [
            SOGE,
        ],
        'plus_arg': 'entre',
        'label': 'Virement depuis le compte ComNPay vers le compte Société Générale',
    },
}

def parse(filename):
    """Parse le fichier ComNpay et produit un csv gérable par hledger"""

    entries = [
        line
        for line in csv.reader(open(filename))
    ][2:]

    with open("/usr/scripts/var/comnpay_parsed.journal", "w") as hledger_journal:
        for entry in entries:
            op_type = entry[TYPE_IDX]

            # La date est sous forme "dd/mm/YYYY HH:MM:SS", on ne veut que le jour/mois/année
            # et on veut inverser le format pour hledger.
            op_date = "/".join(entry[TO_LOOK['date']].split()[0].split("/")[::-1])
            label = USEFUL_DATA[entry[TYPE_IDX]]['label']
            minus = USEFUL_DATA[entry[TYPE_IDX]]['minus']
            minus_arg = USEFUL_DATA[entry[TYPE_IDX]]['minus_arg']
            plus = USEFUL_DATA[entry[TYPE_IDX]]['plus']
            plus_arg = USEFUL_DATA[entry[TYPE_IDX]]['plus_arg']

            # Écrit la date, suivie d'une espace et de la description de l'opération.
            hledger_journal.write(
                "%s %s\n" % (
                    op_date,
                    label,
                ),
            )
            hledger_journal.write(
                "    %s             -%s\n" % (
                    minus[0],
                    entry[TO_LOOK[minus_arg]].replace(",", "."),
                ),
            )

            for sub in plus:
                if sub != plus[-1]:
                    hledger_journal.write(
                        "    %s             %s\n" % (
                            sub,
                            entry[TO_LOOK[plus_arg]].replace(",", "."),
                        ),
                    )
                else:
                    hledger_journal.write(
                        "    %s\n" % (
                            sub,
                        ),
                    )
            hledger_journal.write("\n")



if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Parser le csv des opérations COMNPAY", add_help=False)
    PARSER.add_argument('-h', '--help', action='store_true', help='Affiche cette aide et quitte.')
    PARSER.add_argument('fichier', type=str, nargs=1, help="Chemin du fichier CSV à parser")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    parse(ARGS.fichier[0])
