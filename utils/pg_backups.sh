#!/bin/bash
# Author: Pierre-Elliott Bécue <becue@crans.org>
# Script qui est appelé par backuppc lorsqu'il tente
# de backuper thot

PATH="/sbin:/bin:/usr/sbin:/usr/bin"
destination='/pg_backups/'
mail='Roots <roots@crans.org>'
liste_backups='django etherpad horde5 mediadrop owncloud roundcube sqlgrey switchs'
probs=0

if [ ! -d $destination ]; then
    mkdir -p $destination
fi

for i in $liste_backups; do
    if sudo -u postgres pg_dump $i | bzip2 -z > $destination/$i.sql.bz2; then
        true;
    else
        retour="${retour}\nArchivage de la base $i échoué le $(date)...";
        probs=1
    fi;
done

if [[ $probs -eq 1 ]]; then
(cat << EOF
From: backuppc@crans.org
To: ${mail}
Subject: Backups postgresql foirés
Content-Type: text/plain; charset=UTF-8;

${retour}
EOF
) | sendmail -t
fi;
