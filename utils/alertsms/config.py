# -*- mode: python; coding: utf-8 -*-

"""
    Configuration du service de SMS du Crans
"""

import pika

from gestion import secrets_new as secrets

# Contact avec le daemon Gammu smsd
SMSD_CONF = '/etc/gammu-smsdrc'
SMSC_DEFAULT = {'Location' : 1}

# Paramètres AMQP
CREDS = pika.credentials.PlainCredentials('sms', secrets.get('rabbitmq_sms'), True)
PARAMS = pika.ConnectionParameters(host='rabbitmq.crans.org',
    port=5671, credentials=CREDS, ssl=True)
QUEUE = "SMS"
