#!/usr/bin/python
#-*- coding: utf-8 -*-

"""
    Plugin Nagios vérifiant l'état d'un onduleur via SNMP
    Nécéssite python-netsnmp.
"""

from __future__ import print_function, unicode_literals

import os, sys
from argparse import ArgumentParser
from collections import namedtuple, OrderedDict

import re

import nagiosplugin

from netsnmp import snmpwalk, Varbind

# Définition des arguments que mange le script
parser = ArgumentParser(description=__doc__)
parser.add_argument('-H', '--hostname', type=unicode, required=True, help="Adresse IP ou nom de l'onduleur")
parser.add_argument('-w', '--warning', type=int, nargs='?', default=100, help="Seuil pour la génération d'un avertissement")
parser.add_argument('-c', '--critical', type=int, nargs='?', default=50, help="Seuil pour la génération d'une alerte")
parser.add_argument('-C', '--community', type=unicode, nargs='?', default="public", help="Paramètre 'community' utilisé pour l'authentification en SNMPv1/SNMPv2c")
parser.add_argument('-L', '--legacy', action='store_true', help="Utiliser les MIB legacy pour les onduleurs MGE plutôt que ceux de l'IETF")
parser.add_argument('-B', '--battery', action='store_true', help="Récupérer les informations de la batterie")
parser.add_argument('-I', '--input', action='store_true', help="Récupérer les informations sur l'alimentation électrique en entrée")
parser.add_argument('-O', '--output', action='store_true', help="Récupérer les informations sur l'alimentation électrique en sortie")
parser.add_argument('-E', '--environment', action='store_true', help="Récupérer les informations sur l'environnement de l'onduleur")

if __name__ == "__main__":
    args = parser.parse_args()

# OIDs

OID_BATTERY = {
    'IETF'  : 'upsBattery',
    'MGE'   : 'SNMPv2-SMI::enterprises.705.1.5',
}

OID_INPUT = {
    'IETF'  : 'upsInput',
    'MGE'   : 'SNMPv2-SMI::enterprises.705.1.6',
}

OID_OUTPUT = {
    'IETF'  : 'upsOutput',
    'MGE'   : 'SNMPv2-SMI::enterprises.705.1.7',
}

OID_ENVIRONMENT = {
    'IETF'  : None,
    'MGE'   : 'SNMPv2-SMI::enterprises.705.1.8',
}

# État des batteries

BATTERY_STATUS = {
    1 :   'Inconnu',
    2 :   'Normal',
    3 :   'Faible',
    4 :   'Affabli',
}

# Sources possibles pour l'alimentation en sortie

OUTPUT_SOURCES = {
    1 :   'Autre',
    2 :   'Aucune',
    3 :   'Normal',
    4 :   'Mode Bypass',
    5 :   'Batterie',
    6 :   'Booster',
    7 :   'Reducer',
}

# Classe resprésentant un résultat de mesure
Probe = namedtuple('Probe', ['value', 'uom', 'min', 'max', 'context'])

def get_status_from_OID(oid, hostname, community):
    """
        Procède à un snmpwalk pour l'hôte spécifié et l'OID donné
    """
    return snmpwalk(
        Varbind(oid),
        Version=1,
        DestHost=hostname,
        Community=community
    )

def clean_snmp_output(iterable):
    """
        Retire les caractères indésirables de la sortie de NetSNMP
    """
    try:
        return [x.replace(b"\x00", "") for x in iterable]
    except TypeError:
        return iterable

class StateContext(nagiosplugin.Context):
    """
        Un contexte pour l'évaluation de l'état des batteries
    """
    def evaluate(self, metric, resource):
        if self.name == 'out_state':
            if metric.value in [1, 2]:
                state = nagiosplugin.Unknown
            elif metric.value in [3, 4]:
                state = nagiosplugin.Ok
            elif metric.value in [6, 7]:
                state = nagiosplugin.Warn
            else:
                state = nagiosplugin.Critical
        elif self.name == 'batt_state':
            if metric.value in [1]:
                state = nagiosplugin.Unknown
            elif metric.value in [2]:
                state = nagiosplugin.Ok
            elif metric.value in [3]:
                state = nagiosplugin.Warn
            else:
                state = nagiosplugin.Critical
        else:
            state = nagiosplugin.Unknown

        return self.result_cls(state, self.describe(metric), metric)

    def describe(self, metric):
        if self.name == 'batt_state':
            return BATTERY_STATUS[metric.value]
        elif self.name == 'out_state':
            return OUTPUT_SOURCES[metric.value]
        else:
            return None


class UPS(nagiosplugin.Resource):
    """
        Ressource représentant un onduleur
    """

    def __init__(self, hostname, community,
                 legacy, battery, input,
                 output, environment):
        self.hostname = hostname
        self.community = community
        self.oid_type = 'MGE' if legacy else 'IETF'
        self.battery = battery
        self.input = input
        self.output = output
        self.environment = environment

    def _get(self, what):
        return clean_snmp_output(
                get_status_from_OID(
                    what, self.hostname, self.community
                    )
                )

    def get_battery_state(self):
        b_state = self._get(OID_BATTERY[self.oid_type])

        if self.oid_type == 'IETF':
            res = OrderedDict({
                'Etat' : Probe(int(b_state[0]), None, None, None, 'batt_state'),
                'Autonomie' : Probe(int(b_state[2])*60, 's', 0, None, 'autonomy'),
                'Niveau' : Probe(int(b_state[3]), '%', 0, 100, 'level'),
            })

        else:
            res = OrderedDict({
                'Autonomie' : Probe(int(b_state[0]), 's', 0, None, 'autonomy'),
                'Niveau' : Probe(int(b_state[1]), '%', 0, 100, 'level'),
            })

        return res

    def get_input_state(self):
        in_state = self._get(OID_INPUT[self.oid_type])

        if self.oid_type == 'IETF':
            return OrderedDict({
                'Frequence en entree' : Probe(int(in_state[2])*0.1, 'Hz', None, None, 'frequency'),
                'Tension en entree' : Probe(int(in_state[3]), 'V', None, None, 'voltage'),
                'Courant en entree' : Probe(int(in_state[4])*0.1, 'A', None, None, 'current'),
            })
        else:
            return OrderedDict({
                'Tension en entree' : Probe(int(in_state[1])*0.1, 'V', None, None, 'voltage'),
                'Courant en entree' : Probe(int(in_state[9])*0.1, 'A', None, None, 'current'),
            })

    def get_output_state(self):
        out_state = self._get(OID_OUTPUT[self.oid_type])

        if self.oid_type == 'IETF':
            return OrderedDict({
                'Fonctionnement' : Probe(int(out_state[0]), None, None, None, 'out_state'),
                'Frequence de sortie' : Probe(int(out_state[1])*0.1, 'Hz', None, None, 'frequency'),
                'Tension de sortie' : Probe(int(out_state[3]), 'V', None, None, 'voltage'),
                'Courant de sortie' : Probe(int(out_state[4])*0.1, 'A', None, None, 'current'),
                'Charge' : Probe(int(out_state[6]), '%', 0, None, 'load'),
            })
        else:
            return OrderedDict({
                'Frequence de sortie' : Probe(int(out_state[3])*0.1, 'Hz', None, None, 'frequency'),
                'Tension de sortie' : Probe(int(out_state[2])*0.1, 'V', None, None, 'voltage'),
                'Courant de sortie' : Probe(int(out_state[5]), 'A', None, None, 'current'),
                'Charge' : Probe(int(out_state[4]), '%', 0, None, 'load'),
            })

    def get_environment(self):
        env_state = self._get(OID_ENVIRONMENT[self.oid_type])

        if self.oid_type == 'IETF':
            return {}
        else:
            return {
                'Temperature' : Probe(int(env_state[0])*0.1, 'C', None, None, 'temperature'),
                'Hygrometrie' : Probe(int(env_state[1])*0.1, '%', 0, 100, 'hygrometry'),
            }

    def probe(self):
        probes = OrderedDict()
        if self.battery:
            probes.update(**self.get_battery_state())
        if self.input:
            probes.update(**self.get_input_state())
        if self.output:
            probes.update(**self.get_output_state())
        if self.environment:
            probes.update(**self.get_environment())

        for desc, values in probes.items():
            yield nagiosplugin.Metric(
                desc,
                values.value,
                uom=values.uom,
                min=values.min,
                max=values.max,
                context=values.context
            )

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        UPS(args.hostname, args.community, args.legacy, args.battery, args.input, args.output, args.environment),
        nagiosplugin.ScalarContext('frequency', '45:55', '40:60'),
        nagiosplugin.ScalarContext('voltage', '220:240', '200:260'),
        nagiosplugin.ScalarContext('current', '~:', '~:'),
        nagiosplugin.ScalarContext('load', '~:70', '~:90'),
        nagiosplugin.ScalarContext('temperature', '10:20', '0:30'),
        nagiosplugin.ScalarContext('hygrometry', '65', '85'),
        nagiosplugin.ScalarContext('level', '20:', '5:'),
        nagiosplugin.ScalarContext('autonomy', '1200:', '600:'),
        StateContext('batt_state'),
        StateContext('out_state'),
    )

    check.main()

if __name__ == '__main__':
    os.environ['MIBS'] = 'UPS-MIB:SNMPv2-SMI'
    main()
