#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
import sys

import lc_ldap.shortcuts

conn=lc_ldap.shortcuts.lc_ldap_readonly()
try:
    if len(sys.argv[1])==5:
        aid=int(sys.argv[1][1:])
    else:
       raise ValueError('Pas un numero valide')
except ValueError:
    sys.stdout.write('FALSE')
    exit(0)
if conn.search(u"aid=%s" % aid):
    sys.stdout.write('TRUE')
else:
    sys.stdout.write('FALSE')
