# -*- mode: python; coding: utf-8 -*-

import re
from commands import getstatusoutput,getoutput
import requests

HOST_URL = 'http://autostatus-data.adm.crans.org/hosts'
STATUS_URL = 'http://autostatus-data.adm.crans.org/local.status'

def execute(macro, text) :

    descriptions = {}
    hosts = requests.get(HOST_URL).text
    for line in hosts.split('\n'):
        line = line.split(' ', 4)
        host = line[0]
        try:
            dec = line[4]
        except IndexError:
            dec = u"N/A"
        dec = dec.strip()
        descriptions[host] = u'%s' % host
        if dec:
            descriptions[host] += u' (%s)' % dec

    lines = requests.get(STATUS_URL).text.split('\n')[:-1]

    hosts = macro.formatter
    code = hosts.table(1)

    typlist =  ['routes', 'serveurs', 'serveurs de la ferme', 'switches', 'bornes', u'services extérieurs au crans']

    for typ in typlist:
        lines = lines [1:]
        trucsdown = ''

        while lines and not re.match('dummy_host_\d+ 0 ', lines[0]):
            if not re.search(' 0 ', lines[0]):
                line = lines[0].split(' ')
                trucsdown += hosts.table_row(1)
                trucsdown += hosts.table_cell(1,{'style':'background-color:lightgrey; color: black;'})
                trucsdown += hosts.text(descriptions.get(line[0], line[0]))
                trucsdown += hosts.table_cell(0)
                # nombre de non réponse au ping
                if int(line[1]) > 2 :
                    trucsdown += hosts.table_cell(1,{'style':'background-color:red; color: black;'})
                    trucsdown += hosts.text(u'down')
                else :
                    trucsdown += hosts.table_cell(1,{'style':'background-color:blue;'})
                    trucsdown += hosts.text(u'état incertain')
                trucsdown += hosts.table_cell(0)
                trucsdown += hosts.table_row(0)
            lines = lines [1:]

        if trucsdown == '':
            code += hosts.table_row(1)
            code += hosts.table_cell(1, {'style':'background-color:silver; color: black;'})
            code += hosts.text(u'Autostatus des '+typ)
            code += hosts.table_cell(0)
            code += hosts.table_cell(1, {'style':'background-color:lime; color: black;'})
            code += hosts.text(u'OK')
            code += hosts.table_cell(0)
            code += hosts.table_row(0)
        else:
            code += hosts.table_row(1)
            code += hosts.table_cell(1, {'style':'background-color:silver; color:black;', 'colspan':'2' })
            code += hosts.text(u'Autostatus des '+typ)
            code += hosts.table_cell(0)
            code += hosts.table_row(0)
            code += hosts.table_row(1)
            code += hosts.table_cell(1, {'style':'background-color:aliceblue', 'colspan':'2'})
            code += hosts.table(1) + trucsdown + hosts.table(0)
            code += hosts.table_cell(0)
            code += hosts.table_row(0)
            trucsdown = ''

    code += hosts.table(0)
    return code
