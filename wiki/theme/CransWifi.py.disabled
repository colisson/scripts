# -*- coding: iso-8859-1 -*-
"""
    MoinMoin cr@ns


"""

from MoinMoin import wikiutil, version
from MoinMoin import caching
from MoinMoin.theme import ThemeBase
from MoinMoin.Page import Page
from MoinMoin.widget import html

class Theme(ThemeBase):
    name = "CransWifi"
    scripts = ['jquery-1.3.2.min.js', 'crans.js']
    stylesheets = (
        # media         basename
        ('all',         'reset-fonts-grids'),
        ('all',         'base-min'),
        ('all',         'common'),
        ('screen',      'screen'),
        ('print',       'print'),
        ('projection',  'projection'),
        )

    def header(self, d, **kw):
        """ Assemble wiki header
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        # hack
        self.cfg.logo_string = u'<img src="/wiki/CransWifi/img/logo2.png" alt="cr@ns Logo">'
        html = [
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),
            
            # Header
            u'<div id="doc4" class="yui-t2">',
            u'<div id="hd">',

            u'</div>',
#             u'<div id="locationline">',
#             self.interwiki(d),
#             u'</div>',
#             u'<div id="pageline"><hr style="display:none;"></div>',

#             self.editbar(d),
#             u'</div>',
            u'<div id="bd">',
            u'<div id="yui-main">',
            u'<div class="yui-b"><div class="yui-g PageContent">',
            self.msg(d),
            self.breadcrumbs(d),
            self.title(d),
            
            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),
            
            # Start of page
            self.startPage(),
        ]
        return u'\n'.join(html)

    def footer(self, d, **keywords):
        """ Assemble wiki footer
        
        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            u'<hr />',
            self.pageinfo(page),
            self.endPage(),
            # Pre footer custom html (not recommended!)
            self.emit_custom_html(self.cfg.page_footer1),
            u'</div>',  # end of yui-g 
            u'</div>',  # end of yui-b
            u'</div>',  # end of yui-main
            # NAVIGATION
            u'<div class="yui-b" id=\"left-sidebar\">',  
            self.logo(),
            u'<h2>Navigation</h2>',
            self.navibar(d),
            u'</div>',  # end of yui-b (navigation)
            u'</div>',  # end of #bd
#            self.credits(d),
#            self.showversion(d, **keywords),
            u'</div>',  # end of #doc4
            # Toolbar
            u'<div id="ToolBar">',
            self.editbar(d),
            self.searchform(d),
            u'</div>',
            # UserBar
            u'<div id="UserBar">',
            self.username(d),
            u'</div>',
            # Login
            u'<div id="loginBox">',
            self.loginform(d),
            u'</div>',
            # Post footer custom html
            self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)
    
    def title(self, d):
        """ Assemble the title
        
        @param d: parameter dictionary
        @rtype: string
        @return: title html
        """
        _ = self.request.getText
        page_title = ""
        if d['title_text'] == d['page'].split_title(): 
            curpage = ''
            segments = d['page_name'].split('/') # was: title_text
            page_title = segments[-1]
        else:
            page_title = wikiutil.escape(d['title_text'])
        html = '<h1 class="bigtitle">%s</h1>' % page_title
        return html

    def breadcrumbs(self, d):
        """ Assemble the breadcrumbs
        
        @param d: parameter dictionary
        @rtype: string
        @return: title html
        """
        _ = self.request.getText
        content = []
        if d['title_text'] == d['page'].split_title(): 
            curpage = ''
            segments = d['page_name'].split('/') # was: title_text
            for s in segments[:-1]:
                curpage += s
                content.append("<li>%s</li>" % Page(self.request, curpage).link_to(self.request, s))
                curpage += '/'
            content.append(('<li><span class="disabled">%(text)s</span></li>') % {
                'text': wikiutil.escape(segments[-1]),
                })
        else:
            content.append('<li>%s</li>' % wikiutil.escape(d['title_text']))
        if len(content) < 2 :
            return ""
        html = '<ul id="breadcrumbs">%s</ul>' % "".join(content)
        return html

    def loginform(self, d):
        """ Create the complete HTML form code. """
        def make_field(label, name, value="", type="text", size="32", **kw):
            p = html.P(**kw)
            if label:
                p.append(html.Raw('<label for="%s">%s</label>' % (name, label)))
            p.append(html.INPUT(type=type, value=value, size=size, name=name))
            return p
        _ = self.request.getText
        request = self.request
        sn = request.getScriptname()
        pi = request.getPathinfo()
        action = u"%s%s" % (sn, pi)
        hints = []
        for authm in request.cfg.auth:
            hint = authm.login_hint(request)
            if hint:
                hints.append(hint)
        form = html.FORM(action=action, name="loginform")
        form.append(html.INPUT(type="hidden", name="action", value="login"))
        for hint in hints:
            form.append(html.P().append(html.Raw(hint)))
        cfg = request.cfg
        if 'username' in cfg.auth_login_inputs:
            form.append(
                make_field(label=_('Name'), name="name"))

        if 'password' in cfg.auth_login_inputs:
            form.append(
                make_field(label=_('Password'), name="password", type="password"))

        if 'openid_identifier' in cfg.auth_login_inputs:
            form.append(
                make_field(label=_('OpenID'), name="openid_identifier", id="openididentifier"))
        buttons = html.P(CLASS="buttons").extend([
                html.A(CLASS="hideLoginBox", href="#").extend([html.Raw(_("Cancel"))]), 
                html.Raw('&nbsp;'),
                html.INPUT(name='login', type="submit", value=_('Login'))])
        form.append(buttons)

        return unicode(form)

    def headscript(self, d):
        link = '<script type="text/javascript" src="%s"></script>'

        # Create stylesheets links
        html = []
        prefix = self.cfg.url_prefix_static
        jshref = '%s/%s/js' % (prefix, self.name)
        for script in self.scripts:
            href = '%s/%s' % (jshref, script)
            html.append(link % href)
        html = u"\n".join(html)

        return ThemeBase.headscript(self, d)+html

    # disable gui editor
    def guiEditorScript(self, d):
        return ""

    def editorheader(self, d, **kw):
        """ Assemble wiki header for editor
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        return self.header(d, **kw)


def execute(request):
    """
    Generate and return a theme object
        
    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)

